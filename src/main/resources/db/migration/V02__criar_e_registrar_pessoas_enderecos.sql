CREATE TABLE pessoa (
	codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
	nome        VARCHAR(50) NOT NULL,
	logradouro  VARCHAR(50),
	numero      VARCHAR(50),
	complemento VARCHAR(50),
	bairro      VARCHAR(50),
	cep         VARCHAR(50),
	cidade      VARCHAR(50),
	estado      VARCHAR(50),
	ativo       BOOLEAN NOT NULL DEFAULT TRUE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO pessoa (nome, logradouro, numero, complemento, bairro, cep, cidade, estado, ativo)
 values ('Romero'  , 'Parque São Jorge', 1910, 0901, 'Itaquera', '1977-10', 'São Paulo', 'SP', true);
INSERT INTO pessoa (nome, logradouro, numero, complemento, bairro, cep, cidade, estado, ativo)
 values ('Jadson'  , 'Parque São Jorge', 1910, 0901, 'Itaquera', '1977-10', 'São Paulo', 'SP', true);
INSERT INTO pessoa (nome, logradouro, numero, complemento, bairro, cep, cidade, estado, ativo)
 values ('Danilo'  , 'Parque São Jorge', 1910, 0901, 'Itaquera', '1977-10', 'São Paulo', 'SP', true);
INSERT INTO pessoa (nome, logradouro, numero, complemento, bairro, cep, cidade, estado, ativo)
 values ('Cássio'  , 'Parque São Jorge', 1910, 0901, 'Itaquera', '1977-10', 'São Paulo', 'SP', true);
INSERT INTO pessoa (nome, logradouro, numero, complemento, bairro, cep, cidade, estado, ativo)
 values ('Fagner'  , 'Parque São Jorge', 1910, 0901, 'Itaquera', '1977-10', 'São Paulo', 'SP', true);
INSERT INTO pessoa (nome, logradouro, numero, complemento, bairro, cep, cidade, estado, ativo)
 values ('Ralf'    , 'Parque São Jorge', 1910, 0901, 'Itaquera', '1977-10', 'São Paulo', 'SP', true);
INSERT INTO pessoa (nome, logradouro, numero, complemento, bairro, cep, cidade, estado, ativo)
 values ('Paulinho', 'Parque São Jorge', 1910, 0901, 'Itaquera', '1977-10', 'São Paulo', 'SP', false);
INSERT INTO pessoa (nome, logradouro, numero, complemento, bairro, cep, cidade, estado, ativo)
 values ('Ronaldo', 'Parque São Jorge', 1910, 0901, 'Itaquera', '1977-10', 'São Paulo', 'SP', false);
INSERT INTO pessoa (nome, logradouro, numero, complemento, bairro, cep, cidade, estado, ativo)
 values ('Dida', 'Parque São Jorge', 1910, 0901, 'Itaquera', '1977-10', 'São Paulo', 'SP', false);
INSERT INTO pessoa (nome, logradouro, numero, complemento, bairro, cep, cidade, estado, ativo)
 values ('Rincón', 'Parque São Jorge', 1910, 0901, 'Itaquera', '1977-10', 'São Paulo', 'SP', false);

