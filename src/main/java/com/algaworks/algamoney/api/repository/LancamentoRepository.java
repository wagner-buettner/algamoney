package com.algaworks.algamoney.api.repository;

import com.algaworks.algamoney.api.model.Lancamento;
import com.algaworks.algamoney.api.repository.lancamento.LancamentoRepositoryQuery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface LancamentoRepository extends JpaRepository<Lancamento, Long>, LancamentoRepositoryQuery {

    @Query("    select l " +
           "      from Lancamento l " +
           "join fetch l.categoria c " +
           "join fetch l.pessoa p " +
           "  order by l.codigo")
    List<Lancamento> findAll();
}